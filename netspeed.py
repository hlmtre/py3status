# -*- coding: utf-8 -*-
from time import time
from subprocess import check_output
class Py3status:
  def kill(self, i3status_output_json, i3status_config):
    pass

  def on_click(self, i3status_output_json, i3status_config, event):
    pass

  def netspeed(self, i3status_output_json, i3status_config):
    if not self._isup('br1'):
      text = ""
    else:
      text = self._get_network_bytes('br1')
    response = {'cached_until': time() + 1, 'full_text': text, 'name': 'netspeed', 'instance': 'first'}
    return response

  def _format_bytes(self, bytes_num):
    sizes = [ "b", "kb", "mb", "gb" ]
    i = 0
    dblbyte = bytes_num
    while (i < len(sizes) and bytes_num >= 1024):
      dblbyte = bytes_num / (1024.0)
      i += 1
      bytes_num = bytes_num / (1024)

    return str(round(dblbyte, 2)) + " " + sizes[i]

  def _isup(self, interface):
    f = check_output(('ip link show ' + interface).split())
    if 'state UP' in f:
      return True
    return False

  def _get_network_bytes(self, interface):
    for line in open('/proc/net/dev', 'r'):
      if interface in line:
        data = line.split('%s:' % interface)[1].split()
        rx_bytes, tx_bytes = (data[0], data[8])
        try:
          f = open('/tmp/netspeedbr1', 'r+')
        except IOError:
          f = open('/tmp/netspeedbr1', 'w+')
        line = f.readline()
        prev_rx = 0
        prev_tx = 0
        if line != "":
          prev_rx = line.split()[0]
          prev_tx = line.split()[1]
          f.seek(0)
          f.write(str(rx_bytes) + " " + str(tx_bytes) + "\n")
        else:
          f.seek(0)
          f.write(str(rx_bytes) + " " + str(tx_bytes) + "\n")

# if we assume this is being updated every second... it becomes bits per second.
        if prev_rx == 0 or prev_tx == 0:
          return interface + ": " +  self._format_bytes(rx_bytes) + "v|^" + self._format_bytes(tx_bytes)
        rx = int(rx_bytes) - int(prev_rx)
        tx = int(tx_bytes) - int(prev_tx)
        return interface + ": " + self._format_bytes(rx) + " ↓|↑ " + self._format_bytes(tx)

if __name__ == "__main__":
  """
  Run module in test mode.
  """
  from py3status.module_test import module_test
  module_test(Py3status)
